#include <stdio.h>
#include <stdlib.h>

#include "poly.h"


int main (int argc, char **argv)
{
  p_polyf_t p1, p2, p3, p4, p5, p6, p7, p8 ;
  float res;
  int egal;

  if (argc != 3)
    {
      fprintf (stderr, "deux paramètres (polynomes,fichiers) sont à passer \n") ;
      exit (-1) ;
    }
      
  p1 = lire_polynome_float (argv [1]) ;
  p2 = lire_polynome_float (argv [2]) ;

  ecrire_polynome_float (p1) ;
  ecrire_polynome_float (p2) ;

  printf ("************************Test egalite************************\n \n") ;

  egal = egalite_polynome(p1, p2);

  if (egal==0)
    printf ("Les polynomes ne sont pas égaux \n \n") ;
  else
    printf ("Les polynomes sont égaux \n \n") ;

  printf ("************************Test addition************************\n \n") ;

  p3 = addition_polynome (p1, p2) ;
  ecrire_polynome_float (p3) ;

  printf ("************************Test multiplication 1************************\n \n") ;

  p4 = multiplication_polynome_scalaire (p1, 2);
  ecrire_polynome_float (p4) ;

  printf ("************************Test multiplication 2************************\n \n") ;

  p4 = multiplication_polynomes (p1, p1);
  ecrire_polynome_float (p4) ;


  printf ("************************Test eval************************\n \n") ;

  res = eval_polynome (p1, 2);
  printf ("Le resultat est %f \n", res) ;

  p5 = creer_polynome (4) ;
  p6 = creer_polynome (4) ;

  init_polynome (p5, 1.0) ;
  init_polynome (p6, 1.0) ;

  printf ("************************Test egalite 2************************\n \n") ;

  egal = egalite_polynome(p5, p6);

  if (egal==0)
    printf ("Les polynomes ne sont pas égaux \n \n") ;
  else
    printf ("Les polynomes sont égaux \n \n") ;

  printf ("************************Test puissance************************\n \n") ;

  p7 = puissance_polynome(p1, 2);
  ecrire_polynome_float (p7) ;

  printf ("************************Test composition polynome************************\n \n") ;

  p8 = composition_polynome(p1, p2);
  ecrire_polynome_float (p8) ;

  detruire_polynome (p1);
  detruire_polynome (p2);
  detruire_polynome (p3);
  detruire_polynome (p4);
  detruire_polynome (p5);
  detruire_polynome (p6);
  detruire_polynome (p7);
  detruire_polynome (p8);

  printf ("/////////////////////////////////////////////////////////////////////////////////////////////////////\n \n") ;
  printf ("////////////////////////////////////////Tests polynomes creux////////////////////////////////////////\n \n") ;
  printf ("/////////////////////////////////////////////////////////////////////////////////////////////////////\n \n") ;

  p_polyf_creux_t p9, p10, p11; 

  p9 = lire_polynome_creux_float (argv [1]) ;
  p10 = lire_polynome_creux_float (argv [2]) ;

  ecrire_polynome_creux_float (p9) ;
  ecrire_polynome_creux_float (p10) ;

  printf ("************************Test egalite************************\n \n") ;

  egal = egalite_polynome_creux(p9, p10);

  if (egal==0)
    printf ("Les polynomes ne sont pas égaux \n \n") ;
  else
    printf ("Les polynomes sont égaux \n \n") ;

  printf ("************************Test multiplication scalaire************************\n \n") ;

  p11 = multiplication_polynome_scalaire_creux (p9, 2);
  ecrire_polynome_creux_float (p11) ;

  detruire_polynome_creux (p9);
  detruire_polynome_creux (p10);
  detruire_polynome_creux (p11);
  /*
    ajouter du code pour tester les fonctions
    sur les polynomes
  */
}
