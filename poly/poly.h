/*
  polyf_t   : structure polynome
  p_polyf_t : pointeur sur un polynome
*/

#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))

typedef struct {
  int degre ;
  float *coeff;
} polyf_t, *p_polyf_t;

typedef struct m{
  int degre ;
  float coeff ;
  struct m *suivant;
}monome_t, *p_monome_t;

/*typedef struct monome_t *p_monome_t;
struct monome_t{
  int degre ;
  float coeff ;
  p_monome_t *suivant;
};*/

typedef struct {
  int degre ;
  monome_t *premier;
} polyf_creux_t, *p_polyf_creux_t;


p_polyf_t creer_polynome (int degre) ;

p_polyf_creux_t creer_polynome_creux (int degre) ;

void init_polynome_creux (p_polyf_creux_t p, float x) ;

void init_polynome (p_polyf_t p, float x) ;

void detruire_polynome (p_polyf_t p) ;

void detruire_polynome_creux (p_polyf_creux_t p) ;

p_polyf_t lire_polynome_float (char *nom_fichier) ;

p_polyf_creux_t lire_polynome_creux_float (char *nom_fichier) ;

void ecrire_polynome_float (p_polyf_t p) ;

void ecrire_polynome_creux_float (p_polyf_creux_t p) ;

int egalite_polynome (p_polyf_t p1, p_polyf_t p2) ;

int egalite_polynome_creux (p_polyf_creux_t, p_polyf_creux_t p2) ;

p_polyf_t addition_polynome (p_polyf_t p1, p_polyf_t p2) ;

p_polyf_t multiplication_polynome_scalaire (p_polyf_t p, float alpha) ;

p_polyf_creux_t multiplication_polynome_scalaire_creux (p_polyf_creux_t p, float alpha) ;

float eval_polynome (p_polyf_t p, float x) ;

p_polyf_t multiplication_polynomes (p_polyf_t p1, p_polyf_t p2) ;

p_polyf_t puissance_polynome (p_polyf_t p, int n) ;

p_polyf_t composition_polynome (p_polyf_t p, p_polyf_t q) ;

