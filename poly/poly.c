#include <stdio.h>
#include <stdlib.h>
#include <math.h> 

#include "poly.h"

#include <x86intrin.h>

p_polyf_t creer_polynome (int degre)
{
  p_polyf_t p ;
  
  p = (p_polyf_t) malloc (sizeof (polyf_t)) ;
  p->degre = degre ;

  p->coeff = (float *) malloc ((degre+1) * sizeof (float))  ;

  return p ;
}

p_polyf_creux_t creer_polynome_creux (int degre)
{
  p_polyf_creux_t p ;
  
  p =(p_polyf_creux_t) malloc (sizeof (polyf_creux_t)) ;

  p->degre = degre ;

  p->premier = (p_monome_t) malloc (sizeof (monome_t))  ;

  return p ;
}

void detruire_polynome (p_polyf_t p)
{
  free (p->coeff) ;
  free (p) ;

  return ;
}

void detruire_polynome_creux (p_polyf_creux_t p)
{
  free (p->premier) ;
  free (p) ;

  return ;
}

void init_polynome (p_polyf_t p, float x)
{
  register unsigned int i ;

  for (i = 0 ; i <= p->degre; ++i)
    p->coeff [i] = x ;

  return ;
}

void init_polynome_creux (p_polyf_creux_t p, float x)
{
  register unsigned int i ;
  p_monome_t toIterOn = p->premier;
  int degre = 0;
  for (i = 0 ; i <= p->degre; ++i){
    toIterOn->coeff = x ;
    toIterOn->degre = degre ;
    toIterOn->suivant = (p_monome_t) malloc (sizeof (monome_t))  ;
    toIterOn= toIterOn->suivant;
    degre++;
  }
  return ;
}

p_polyf_t lire_polynome_float (char *nom_fichier)
{
  FILE *f ;
  p_polyf_t p ;
  int degre ;
  int i  ;
  int cr ;
  
  f = fopen (nom_fichier, "r") ;
  if (f == NULL)
    {
      fprintf (stderr, "erreur ouverture %s \n", nom_fichier) ;
      exit (-1) ;
    }
  
  cr = fscanf (f, "%d", &degre) ;
  if (cr != 1)
    {
      fprintf (stderr, "erreur lecture du degre\n") ;
      exit (-1) ;
    }
  p = creer_polynome (degre) ;
  
  for (i = 0 ; i <= degre; i++)
    { 
      cr = fscanf (f, "%f", &p->coeff[i]) ;
      if (cr != 1)
      {
          fprintf (stderr, "erreur lecture coefficient %d\n", i) ;
          exit (-1) ;
      }
       
    }

  fclose (f) ;

  return p ;
}

p_polyf_creux_t lire_polynome_creux_float (char *nom_fichier)
{
  FILE *f ;
  p_polyf_creux_t p ;
  int degre ;
  int i  ;
  int cr ;
  float contain ;
  
  f = fopen (nom_fichier, "r") ;
  if (f == NULL)
    {
      fprintf (stderr, "erreur ouverture %s \n", nom_fichier) ;
      exit (-1) ;
    }
  
  cr = fscanf (f, "%d", &degre) ;
  if (cr != 1)
    {
      fprintf (stderr, "erreur lecture du degre\n") ;
      exit (-1) ;
    }
  p = creer_polynome_creux (degre) ;

  p_monome_t toIterOn = p->premier;
  
  for (i = 0 ; i <= degre; i++)
    { 
      cr = fscanf (f, "%f", &contain) ;
      if (cr != 1)
      {
        fprintf (stderr, "erreur lecture coefficient %d\n", i) ;
        exit (-1) ;
      }
      if (contain != 0){
        printf ("contain vaut %f  et i %d \n", contain, i) ;
        toIterOn->coeff = contain;
        toIterOn->degre = i;
        toIterOn->suivant = (p_monome_t) malloc (sizeof (monome_t))  ;
        toIterOn = toIterOn ->suivant;
      }
    }

  fclose (f) ;

  return p ;
}

void ecrire_polynome_creux_float (p_polyf_creux_t p)
{
  p_monome_t toIterOn = p->premier;

  if (toIterOn->degre == 0){
    printf ("%f ", toIterOn->coeff) ; 
    toIterOn = toIterOn->suivant;
  }
  if (toIterOn->degre == 1){
    printf ("+ %f x ", toIterOn->coeff) ;
    toIterOn = toIterOn->suivant;
  }
  if (toIterOn == p->premier){
    printf ("%f X^%d ", toIterOn->coeff, toIterOn->degre) ;
  }
  else{
    printf ("+ %f X^%d ", toIterOn->coeff, toIterOn->degre) ;
  }
  do
  { 
    toIterOn = toIterOn->suivant;
    printf ("+ %f X^%d ", toIterOn->coeff, toIterOn->degre) ;
      
  }
  while(toIterOn->degre < p->degre);
  
  printf ("\n") ;

  return ;
}

void ecrire_polynome_float (p_polyf_t p)
{
  int i ;

  printf ("%f + %f x ", p->coeff [0], p->coeff [1]) ;
  
  for (i = 2 ; i <= p->degre; i++)
    {  
      printf ("+ %f X^%d ", p->coeff [i], i) ;
    }
  
  printf ("\n") ;

  return ;
}

int egalite_polynome (p_polyf_t p1, p_polyf_t p2)
{
  /*
    tester les deux polynomes p1 et p2
  */
  if (p1->degre != p2->degre) return 0;
  register unsigned int i ;
  for (i = 0 ; i <= p1->degre; ++i)
    if (p1->coeff[i] != p2->coeff[i]) return 0;
  return 1 ;
}

int egalite_polynome_creux (p_polyf_creux_t p1, p_polyf_creux_t p2)
{
  /*
    tester les deux polynomes p1 et p2
  */
  if (p1->degre != p2->degre) return 0;
  p_monome_t p_deux_monome = p2->premier;
  p_monome_t p_un_monome = p1->premier;
  while (p_un_monome -> suivant != NULL){
    if (p_deux_monome->coeff != p_un_monome ->coeff) return 0;
    if (p_deux_monome->degre != p_un_monome ->degre) return 0;
    p_deux_monome = p_deux_monome->suivant;
    p_un_monome = p_un_monome->suivant;
  }
  return 1 ;
}


p_polyf_t addition_polynome (p_polyf_t p1, p_polyf_t p2)
{
  p_polyf_t p3 ;
  register unsigned int i ;
  
  p3 = creer_polynome (max (p1->degre, p2->degre));

  for (i = 0 ; i <= min (p1->degre, p2->degre); ++i)
    {
      p3->coeff [i] = p1->coeff [i] + p2->coeff [i] ;
    }

  if (p1->degre > p2->degre)
    {
      for (i = (p2->degre + 1) ; i <= p1->degre; ++i)
	p3->coeff [i] = p1->coeff [i] ;
    }
  else if (p2->degre > p1->degre)
    {
      for (i = (p1->degre + 1) ; i <= p2->degre; ++i)
	p3->coeff [i] = p2->coeff [i] ;
    }
    
  return p3 ;
}

p_polyf_t multiplication_polynome_scalaire (p_polyf_t p, float alpha)
{
  /* alpha * p1 */
  register unsigned int i ;
  p_polyf_t p2 ;
  p2 = creer_polynome (p->degre);
  for (i = 0 ; i <= p->degre; ++i)
    p2->coeff[i] = p->coeff[i]*alpha ;

  return p2;
}

p_polyf_creux_t multiplication_polynome_scalaire_creux (p_polyf_creux_t p, float alpha)
{
  /* alpha * p1 */
  p_polyf_creux_t p2 ;
  p2 = creer_polynome_creux (p->degre);
  p_monome_t p_deux_monome = p2->premier;
  p_monome_t p_un_monome = p->premier;
  while (p_un_monome -> suivant != NULL){
    p_deux_monome ->coeff = p_un_monome ->coeff * alpha;
    p_deux_monome ->degre = p_un_monome ->degre;
    p_deux_monome ->suivant = (p_monome_t) malloc (sizeof (monome_t))  ;
    p_deux_monome = p_deux_monome->suivant;
    p_un_monome = p_un_monome->suivant;
  }
  return p2;
}


float eval_polynome (p_polyf_t p, float x)
{
  /* 
     valeur du polynome pour la valeur de x
  */
  float res = 0;
  register unsigned int i ;
  for (i = 0 ; i <= p->degre; ++i)
    res += p->coeff[i]*(pow(x,i));
  return res ;
}

p_polyf_t multiplication_polynomes (p_polyf_t p1, p_polyf_t p2)
{
  /* p1 * p2 */
  register unsigned int i,j;
  p_polyf_t p3 ;
  p3 = creer_polynome (p1->degre + p2->degre);
  for (i = 0 ; i <= p1->degre; ++i) {
    for (j = 0; j <= p2->degre; ++j){
      p3->coeff[i+j] = p1->coeff[i]*p2->coeff[j];
    }
  }
  return p3;
}

p_polyf_t puissance_polynome (p_polyf_t p, int n)
{
  /* 
     p^n
  */
  p_polyf_t p2 ;
  if (n==0){
    p2 = creer_polynome (0);
    p2->coeff[0] = 1;
    return p2;
  }
  if (n==1)
    return p;
  p2 = creer_polynome (p->degre);
  for (unsigned int i = 0 ; i <= p->degre; ++i)
    p2->coeff[i] = p->coeff[i];
  for (unsigned int j = 1; j < n; j++){
    p2 = multiplication_polynomes(p, p2);
  }
  return p2 ;
}

p_polyf_t composition_polynome (p_polyf_t p, p_polyf_t q)
{
  /*
    x^² +2x +1   x+1
    p O q
  */
  register unsigned int i;
  p_polyf_t r ;
  r = creer_polynome (p->degre * q->degre);
  for (i = 0 ; i <= p->degre; ++i) {
    r = addition_polynome(r, multiplication_polynome_scalaire(puissance_polynome(q,i), p->coeff[i])); //à modifier
  }
  return r ;
}



