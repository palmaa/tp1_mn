#include <stdio.h>
#include <stdlib.h>

#include "poly.h"

#include <x86intrin.h>

static const float duree_cycle = (float) 1 / (float) 2.6 ; // duree du cycle en nano seconde 10^-9

void calcul_flop (char *message, int nb_operations_flottantes, unsigned long long int cycles)
{
  printf ("%s %d operations %f GFLOP/s\n", message, nb_operations_flottantes, ((float) nb_operations_flottantes) / (((float) cycles) * duree_cycle)) ;
  return ;
}

int main (int argc, char **argv)
{
  p_polyf_t p1, p2, p3, p4, p5, p6;
  p_polyf_creux_t p7, p8, p9, p10, p11; 
  float resultat;
  int egal;
  unsigned long long start, end ;
  
  if (argc != 3)
    {
      fprintf (stderr, "deux paramètres (polynomes,fichiers) sont à passer \n") ;
      exit (-1) ;
    }
      
  p1 = lire_polynome_float (argv [1]) ;
  p2 = lire_polynome_float (argv [2]) ;

  p7 = lire_polynome_creux_float (argv [1]) ;
  p8 = lire_polynome_creux_float (argv [2]) ;

  printf ("p1 = ") ;
  ecrire_polynome_float (p1) ;

  printf ("p2 = ") ;
  ecrire_polynome_float (p2) ;

  /*
    ajouter du code pour tester les fonctions
    sur les polynomes
  */

// tests sur l'addition

  printf ("************************Test addition 1************************\n \n") ;

  start = _rdtsc () ;
  
        p3 = addition_polynome (p1, p2) ;

  end = _rdtsc () ;

  printf ("p3 = ") ;
  ecrire_polynome_float (p3) ;
  
  printf ("addition %Ld cycles\n", end-start) ;
  calcul_flop ("p1+p2", min(p1->degre, p2->degre)+1, end-start) ;
  detruire_polynome (p3) ;

  p4 = creer_polynome (1024) ;
  p5 = creer_polynome (1024) ;

  init_polynome (p4, 1.0) ;
  init_polynome (p5, 2.0) ;


  printf ("************************Test addition 2************************\n \n") ;

  start = _rdtsc () ;
  
        p6 = addition_polynome (p4, p5) ;

  end = _rdtsc () ;

  printf ("addition %Ld cycles\n", end-start) ;
  calcul_flop ("p4+p5", min(p4->degre, p5->degre)+1, end-start) ;
  detruire_polynome (p6) ;

// tests sur la multiplication

  printf ("************************Test multiplication************************\n \n") ;

  start = _rdtsc () ;
  
        p3 = multiplication_polynomes (p1, p1) ;

  end = _rdtsc () ;

  printf ("p3 = ") ;
  ecrire_polynome_float (p3) ;
  
  printf ("multiplication %Ld cycles\n", end-start) ;
  calcul_flop ("p1*p1", min(p1->degre, p1->degre)+1, end-start) ;
  detruire_polynome (p3) ;

// tests sur la multiplication scalaire

  printf ("************************Test multiplication scalaire************************\n \n") ;

  start = _rdtsc () ;
  
        p3 = multiplication_polynome_scalaire (p1, 2) ;

  end = _rdtsc () ;

  printf ("p3 = ") ;
  ecrire_polynome_float (p3) ;
  
  printf ("multiplication scalaire %Ld cycles\n", end-start) ;
  calcul_flop ("p1*2", p1->degre+1, end-start) ;
  detruire_polynome (p3) ;

  // tests sur la puissance

  printf ("************************Test puissance************************\n \n") ;

  start = _rdtsc () ;
  
        p3 = puissance_polynome (p1, 2) ;

  end = _rdtsc () ;

  printf ("p3 = ") ;
  ecrire_polynome_float (p3) ;
  
  printf ("puissance %Ld cycles\n", end-start) ;
  calcul_flop ("p1^2", p1->degre+1, end-start) ;
  detruire_polynome (p3) ;

    // tests sur l'évaluation

  printf ("************************Test evaluation************************\n \n") ;
  start = _rdtsc () ;
  
        resultat = eval_polynome (p1, 2) ;

  end = _rdtsc () ;

  printf ("Le resultat est %f \n", resultat) ;
  
  printf ("eval %Ld cycles\n", end-start) ;
  calcul_flop ("p1 avec x=2", p1->degre+1, end-start) ;

  // tests sur la composition


  printf ("************************Test composition************************\n \n") ;

  start = _rdtsc () ;
  
        p3 = composition_polynome (p1, p2) ;

  end = _rdtsc () ;

  printf ("p3 = ") ;
  ecrire_polynome_float (p3) ;
  
  printf ("composition %Ld cycles\n", end-start) ;
  calcul_flop ("p1 O p2", min(p1->degre, p1->degre)+1, end-start) ;
  detruire_polynome (p3) ;

  //--------------------------------------------Tests polynomes creux----------------------------------------------------


  printf ("/////////////////////////////////////////////////////////////////////////////////////////////////////\n \n") ;
  printf ("////////////////////////////////////////Tests polynomes creux////////////////////////////////////////\n \n") ;
  printf ("/////////////////////////////////////////////////////////////////////////////////////////////////////\n \n") ;


  printf ("p7 = ") ;
  ecrire_polynome_creux_float (p7) ;

  printf ("p8 = ") ;
  ecrire_polynome_creux_float (p8) ;

  printf ("************************Test egalite 1************************\n \n") ;

  start = _rdtsc () ;
  
        egal = egalite_polynome_creux(p7, p8);

  end = _rdtsc () ;

  if (egal==0)
    printf ("Les polynomes ne sont pas égaux \n \n") ;
  else
    printf ("Les polynomes sont égaux \n \n") ;
  
  printf ("égalité %Ld cycles\n", end-start) ;
  calcul_flop ("p1 = p2", min(p1->degre, p1->degre)+1, end-start) ;

  p10 = creer_polynome_creux (3) ;
  p11 = creer_polynome_creux (3) ;

  init_polynome_creux (p10, 1.0) ;
  init_polynome_creux (p11, 2.0) ;

  printf ("p10 = ") ;
  ecrire_polynome_creux_float (p10) ;

  printf ("p11 = ") ;
  ecrire_polynome_creux_float (p11) ;

  printf ("************************Test egalite 2************************\n \n") ;

  start = _rdtsc () ;
  
        egal = egalite_polynome_creux(p10, p11);

  end = _rdtsc () ;

  if (egal==0)
    printf ("Les polynomes ne sont pas égaux \n \n") ;
  else
    printf ("Les polynomes sont égaux \n \n") ;
  
  printf ("égalité %Ld cycles\n", end-start) ;
  calcul_flop ("p10 = p11", min(p10->degre, p11->degre)+1, end-start) ;

  printf ("************************Test multiplication scalaire************************\n \n") ;

  start = _rdtsc () ;
  
        p9 = multiplication_polynome_scalaire_creux (p7, 2);

  end = _rdtsc () ;

  printf ("p9 = ") ;
  ecrire_polynome_creux_float (p9) ;
  
  printf ("multiplication scalaire %Ld cycles\n", end-start) ;
  calcul_flop ("p1 * 2", p1->degre+1, end-start) ;
  detruire_polynome_creux (p9) ;


 
}
